import graphene

from graphene_django.types import DjangoObjectType
from .models import Product


class ProductType(DjangoObjectType):
    class Meta:
        model = Product


class Query(graphene.ObjectType):
    allProduct = graphene.List(ProductType)
    product = graphene.Field(ProductType, id=graphene.Int())

    def resolve_allProduct(self, info, **kwargs):
        return Product.objects.all()

    def resolve_product(self, info, id):
        return Product.objects.get(pk=id)


class CartMutation(graphene.Mutation):
    isAddToCart = graphene.Boolean()

    class Arguments:
        # The input arguments for this mutation
        isAddToCart = graphene.Boolean()
        id = graphene.ID()
    # The class attributes define the response of the mutation
    product = graphene.Field(ProductType)

    def mutate(self, info, isAddToCart, id):
        product = Product.objects.get(pk=id)
        product.isAddToCart = isAddToCart
        product.save()
        # Notice we return an instance of this mutation
        return CartMutation(product=product)


class FollowMutation(graphene.Mutation):
    isFollow = graphene.Boolean()

    class Arguments:
        # The input arguments for this mutation
        isFollow = graphene.Boolean()
        id = graphene.ID()
    # The class attributes define the response of the mutation
    product = graphene.Field(ProductType)

    def mutate(self, info, isFollow, id):
        product = Product.objects.get(pk=id)
        product.isFollow = isFollow
        product.save()
        # Notice we return an instance of this mutation
        return FollowMutation(product=product)


class Mutation(graphene.ObjectType):
    updateCart = CartMutation.Field()
    updateFollow = FollowMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
