from django.contrib import admin
from .models import Product

# Register your models here.

class ProductAdmin(admin.ModelAdmin):
  list_display = ('name', 'subname', 'description', 'value', 'price', 'oldPrice', 'discount', 'isAddToCart', 'isFollow', 'more')

admin.site.register(Product, ProductAdmin)
