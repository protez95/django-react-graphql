from django.db import models

# Create your models here.
class Product(models.Model):
  name = models.CharField(max_length=50)
  subname = models.CharField(max_length=50)
  description = models.CharField(max_length=200)
  value = models.CharField(max_length=10)
  price = models.CharField(max_length=50)
  oldPrice = models.CharField(max_length=50, blank=True)
  discount = models.CharField(max_length=10, blank=True)
  isAddToCart = models.BooleanField(default=False)
  isFollow = models.BooleanField(default=False)
  more = models.CharField(max_length=200, blank=True)



