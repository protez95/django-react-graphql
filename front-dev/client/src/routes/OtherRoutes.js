import BlackHolePage from 'pages/BlackHolePage';

const Routes = [
  {
    path: '*',
    name: 'BLACK-HOLE-PAGE',
    component: BlackHolePage,
    exact: false
  }
];

export default Routes;
