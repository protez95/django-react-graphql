import React from 'react';
import {AlertS, Cross, Icon, Statement, Car, Change} from './ProductAlert.style';

class ProductAlert extends React.Component {
  render() {
    return (
      <>
      <AlertS>
        <Cross href="">X</Cross>
        <Icon>✓</Icon>
        <Statement>
          Produkt dopasowany do wybranego samochodu:
          <Car>Mercedes S500 (8E2, B6) 1.8 T quattro, 140 PS, 103 kW</Car>
        </Statement>
        <Change href="">ZMIEŃ</Change>
      </AlertS>

      <AlertS>
        <Cross href="">X</Cross>
        <Icon>!</Icon>
        <Statement>
          Produkt dopasowany do wybranego samochodu:
          <Car>Mercedes S500 (8E2, B6) 1.8 T quattro, 140 PS, 103 kW</Car>
        </Statement>
        <Change href="">ZMIEŃ</Change>
      </AlertS>
      </>
    );
  }
}

export default ProductAlert;
