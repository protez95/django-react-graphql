import styled from 'styled-components';
import { Colors } from 'assets/Variables.style';

export const AlertS = styled.div`
  /* Positioning */
  margin-left: 20px;
  margin-top: 20px;
  /* Display & Box Model */
  height: 105px;
  width: 370px;
  padding: 0.1em;
  display: grid;
  grid-template-columns: 45px 250px 60px 15px;
  grid-template-rows: 70px 25px;
  /* Color */
  background: ${Colors.mercury};
  /* Text */
  font-family: 'Roboto', sans-serif;
  /* Other */
`;

export const Cross = styled.a`
  grid-column: 4;
  grid-row:1;
  color: ${Colors.blackAlfa8};;
  text-decoration: none;
  font-size: 1em;
  font-weight: bold;
`;

export const Icon = styled.div`
  margin: 0.1em;
  margin-bottom: 0.9em;
  border-radius: 0.2em;
  grid-column: 1;
  grid-row:1;
  color: ${Colors.white};
  background: ${Colors.blackAlfa8};
  text-decoration: none;
  text-align: center;
  font-size: 2em;
  font-weight: bold;
`;

export const Statement = styled.div`
  margin-top: 0.4em;
  margin-left: 0.4em;
  grid-column: 2/4;
  grid-row:1;
  line-height: 1.3;
  font-size: 0.9em;
`;

export const Car = styled.div`
  line-height: 1.3;
  font-weight: bold;
  font-size: 1em;
`;

export const Change = styled.a`
  padding: 0.2em 0.5em;
  border-radius: 0.2em;
  grid-column: 3;
  grid-row:2;
  color: ${Colors.white};
  background: ${Colors.scorpion};
  text-align: center;
  font-size: 1em;
  text-decoration: none;
`;
