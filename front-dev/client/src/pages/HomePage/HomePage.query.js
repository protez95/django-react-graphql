import gql from 'graphql-tag';

export const GET_PRODUCT_ITEM_DETAILS = gql`
  {
    product(id: 4) {
      id
      name
      subname
      description
      value
      price
      oldPrice
      discount
      more
      isAddToCart
      isFollow
    }
  }
`;
export const GET_PRODUCT_ITEMS_DETAILS = gql`
  {
    allProduct {
      id
      name
      subname
      description
      value
      price
      oldPrice
      discount
      more
      isAddToCart
      isFollow
    }
  }
`;
export const EDIT_CART_ITEM = gql`
  mutation updateCart($isAddToCart: Boolean!, $id: ID!) {
    updateCart(isAddToCart: $isAddToCart, id: $id) {
      isAddToCart
    }
  }
`;
export const EDIT_FOLLOW_ITEM = gql`
  mutation updateFollow($isFollow: Boolean!, $id: ID!) {
    updateFollow(isFollow: $isFollow, id: $id) {
      isFollow
    }
  }
`;
